import math


class Shape():
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def drow(self):
        pass

    def get_center(self):
        return self.x, self.y

    def __str__(self):
        return f'Shape with center {self.x} {self.y}'


class EquilateralTriangle(Shape):
    def __init__(self, x, y, a):
        super().__init__(x, y)
        self.a = a

    def get_square(self):
        return self.a * self.a * math.sqrt(3) / 4

    def __str__(self):
        return f'EquilateralTriangle with center {self.x} {self.y} and length of all sides {self.a}'


class Rectangle(Shape):
    def __init__(self, x, y, a, b):
        super().__init__(x, y)
        self.a = a
        self.b = b

    def get_square(self):
        return self.a * self.b

    def __str__(self):
        return f'Rectangle with center {self.x} {self.y} and length of all side {self.a} and {self.b}'


class Circle(Shape):

    def __init__(self, x, y, r):
        super().__init__(x, y)
        self.r = r

    def get_square(self):
        return math.pi * (self.r ** 2)

    def __str__(self):
        return f'Circle with center {self.x} {self.y} and radius {self.r}'


class ShapeContainer:
    def __init__(self):
        self.shapes = []

    def append(self, shape):
        self.shapes.append(shape)

    def pop(self):
        return self.shapes.pop()

    def size(self):
        return len(self.shapes)

    def get_area_shapes(self):
        area = 0
        for shape in self.shapes:
            area += shape.get_square()
        return area


shapes = ShapeContainer()
shapes.append(EquilateralTriangle(4, 3, 8))
shapes.append(EquilateralTriangle(2, 2, 5))
shapes.append(Rectangle(2, 5, 2, 5))
shapes.append(Circle(2, 3, 4))
shapes.append(Circle(0, 0, 6))


print(shapes.get_area_shapes(), shapes.size())
